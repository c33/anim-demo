package com.chen.adminDemo;

import androidx.annotation.DrawableRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

  private RecyclerView mRvGrid;

  private Context mContext;
  private ArrayList<Animation> mAnimations;
  private @DrawableRes
  int[] mImages = {
    R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground,
    R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground,
    R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground,
    R.drawable.ic_launcher_background, R.drawable.ic_launcher_foreground,
    R.drawable.ic_launcher_background
  };
  private @DrawableRes int mFrames = R.drawable.anim_images; // 帧动画图像
  private String[] mTexts = {
    "平移", "缩放", "旋转", "透明", "混合",
    "自定", "帧动", "Wrapper", "差值"
  };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

      mRvGrid=findViewById(R.id.main_rv_grid);
      mContext = getApplicationContext();  // 上下文

      initAnimations(mContext);  // 创建动画组合

      mRvGrid.setLayoutManager(new GridLayoutManager(mContext, 2)); // 每行两列
      mRvGrid.setAdapter(new GridAdapter(mContext, mAnimations, mFrames, mTexts, mImages));  // 设置适配器

    }

  private void initAnimations(Context context) {
    mAnimations = new ArrayList<>();
    mAnimations.add(AnimationUtils.loadAnimation(context, R.anim.anim_translate));  // 平移动画
    mAnimations.add(AnimationUtils.loadAnimation(context, R.anim.anim_scale));  // 缩放动画
    mAnimations.add(AnimationUtils.loadAnimation(context, R.anim.anim_rotate));  // 旋转动画
    mAnimations.add(AnimationUtils.loadAnimation(context, R.anim.anim_alpha));  // 透明动画
    mAnimations.add(AnimationUtils.loadAnimation(context, R.anim.anim_all));  // 动画合集

    final Rotate3dAnimation anim = new Rotate3dAnimation(0.0f, 720.0f, 100.0f, 100.0f, 0.0f, false);
    anim.setDuration(2000);
    mAnimations.add(anim);  // 自定义动画
  }


}
